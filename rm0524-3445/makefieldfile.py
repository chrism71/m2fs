from astropy.table import Table
import numpy as np
import os
from astropy.coordinates import SkyCoord
import astropy.units as u

## these can be a table, dictionary, dataframe as long as it takes a name argument and gives a vector of values

## cluster members is table with magnitude,ra,dec,target_name columns
clustmagname = 'RMAG'
clustRAname,clustDECname = 'RA','DEC'
clustpriorityname = 'PRIORITY'

## each star is table with magnitude,ra,dec,target_name columns
starRAname,starDECname = 'RA','DEC'

## each sky is table with magnitude,ra,dec,target_name columns
skyRAname,skyDECname = 'RA','DEC'

def make_files(fieldname,clustername,shstar,gdstars,alstars,skylocs,clustermembers):
    ids = ["GAL%03d" % x for x in np.arange(len(clustermembers))]
    with open('./'+fieldname+'.field','w') as fil:
        with open('./labelmapping.cluster-'+clustername+'_'+fieldname+'.csv','w') as mapper:
            fil.write('Field = %s\n' % fieldname)
            fil.write('minsky = 6\n')
            fil.write('RA DEC EPOCH ID TYPE PRIORITY MAG\n')
            fil.write('%.5f %.5f 2000.0 %s %s %0.1f %0.03f\n' %\
                          (shstar[starRAname],shstar[starDECname],'STAR00','C',9.9,shstar[starmagname]))
            mapper.write('DES_ID,label\n')
            mapper.write('%s,%s\n' %(clustername,fieldname))
            mapper.write('%s,STAR00\n' %(shstar[startargidname]))
            itter = 1
            for i in range(len(gdstars)):
                fil.write('%.5f %.5f 2000.0 %s %s %0.1f %0.03f\n' %(gdstars[starRAname][i],gdstars[starDECname][i],'STAR%02d' % itter,'G',7.,gdstars[starmagname][i]))
                mapper.write('%s,STAR%02d\n' %(gdstars[startargidname][i],itter))
                itter+=1
            for i in range(len(alstars)):
                fil.write('%.5f %.5f 2000.0 %s %s %0.1f %0.03f\n' %(alstars[starRAname][i],alstars[starDECname][i],'STAR%02d' % itter,'A',3.,alstars[starmagname][i]))
                mapper.write('%s,STAR%02d\n' %(alstars[startargidname][i],itter))
                itter+=1
            for skytter,loc in enumerate(skylocs):
                fil.write('%.5f %.5f 2000.0 %s %s %0.1f %0.03f\n' %(skylocs[skyRAname],skylocs[skyDECname],'SKY%03d' % skytter,'S',2.,30.))
            for i in range(len(clustermembers)):
                fil.write('%.5f %.5f 2000.0 %s %s %0.1f %0.03f\n' %(clustermembers[clustRAname][i],clustermembers[clustDECname][i],\
                                                                    ids[i],'T'clustermembers[clustpriorityname][i]))
